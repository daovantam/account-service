package com.mor.accountservice.validator.constraint;

import com.mor.accountservice.contant.RegexConstant;
import com.mor.accountservice.validator.EmailValid;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CheckEmailValid implements ConstraintValidator<EmailValid, String> {

  @Override
  public boolean isValid(String email, ConstraintValidatorContext constraintValidatorContext) {
    if (email == null || email.isEmpty()) {
      return true;
    }
    return email.matches(RegexConstant.EMAIL);
  }
}
