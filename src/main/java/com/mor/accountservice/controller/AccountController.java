package com.mor.accountservice.controller;

import com.mor.accountservice.dto.UserDto;
import com.mor.accountservice.dto.UserRegistrationDto;
import com.mor.accountservice.factory.ResponseFactory;
import com.mor.accountservice.service.AccountService;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {

  private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

  @Autowired
  private ResponseFactory responseFactory;

  private final AccountService accountService;

  public AccountController(AccountService accountService) {
    this.accountService = accountService;
  }

  @PostMapping("/create")
  public ResponseEntity<?> createNewAccount(@RequestBody @Valid UserRegistrationDto user) {

    LOGGER.info("START create new user with data = {}", user);

    UserDto data = accountService.create(user);

    LOGGER.info("END create new user");
    return responseFactory.success(data, UserDto.class);
  }

  @GetMapping("/info")
  public String getInfo() {
    return "protected resource";
  }

  @GetMapping("/reset-password/{username}")
  public ResponseEntity<?> resetPassword(@PathVariable("username") String username) {
    accountService.resetPassword(username);
    return responseFactory.success();
  }

}
