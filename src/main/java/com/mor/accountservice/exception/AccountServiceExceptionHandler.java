package com.mor.accountservice.exception;

import com.mor.accountservice.contant.ResponseStatusCodeEnum;
import com.mor.accountservice.factory.GeneralResponse;
import com.mor.accountservice.factory.ResponseStatus;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class AccountServiceExceptionHandler extends ResponseEntityExceptionHandler {

  private static final Logger logger =
      LoggerFactory.getLogger(AccountServiceExceptionHandler.class);

  /**
   * Handle ConstraintViolationException. Happens when request JSON is malformed.
   *
   * @param ex ConstraintViolationException
   * @return the ResponseEntity<> object
   */
  @ExceptionHandler(ConstraintViolationException.class)
  public ResponseEntity<?> handleConstraintViolationException(ConstraintViolationException ex) {
    logger.error("ConstraintViolationException: ", ex);
    StringBuilder message = new StringBuilder();
    Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
    for (ConstraintViolation<?> violation : violations) {
      Path path = violation.getPropertyPath();
      String[] pathArr = StringUtils.splitByWholeSeparatorPreserveAllTokens(path.toString(), ".");
      message.append(pathArr[1]).append(violation.getMessage());
    }
    message = new StringBuilder(message.substring(0, message.length() - 1));
    return this.createErrorResponse(
        ResponseStatusCodeEnum.INVALID_PARAMETER,
        HttpStatus.BAD_REQUEST,
        message.toString() + ",");
  }

  /**
   * Handle HttpMessageNotReadableException. Happens when request JSON is malformed.
   *
   * @param ex      HttpMessageNotReadableException
   * @param headers HttpHeaders
   * @param status  HttpStatus
   * @param request WebRequest
   * @return the ResponseEntity<> object
   */
  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(
      HttpMessageNotReadableException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    logger.error("HttpMessageNotReadableException: ", ex);
    return this.createErrorResponse(
        ResponseStatusCodeEnum.INVALID_PARAMETER, HttpStatus.BAD_REQUEST, ex.getLocalizedMessage());
  }

  /**
   * Handle HttpMediaTypeNotSupportedException. Happens when request JSON is malformed.
   *
   * @param ex      HttpMediaTypeNotSupportedException
   * @param headers HttpHeaders
   * @param status  HttpStatus
   * @param request WebRequest
   * @return ResponseEntity<> object
   */
  @Override
  protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
      HttpMediaTypeNotSupportedException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    StringBuilder builder = new StringBuilder();
    builder.append(ex.getContentType());
    builder.append("Media type is not supported. Supported media types are ");
    ex.getSupportedMediaTypes().forEach(t -> builder.append(t).append(", "));
    logger.error("HttpMediaTypeNotSupportedException:  ", ex);
    return this.createErrorResponse(
        ResponseStatusCodeEnum.INVALID_PARAMETER,
        HttpStatus.UNSUPPORTED_MEDIA_TYPE,
        builder.toString());
  }

  /**
   * Handle MethodArgumentNotValidException. Happens when request JSON is malformed.
   *
   * @param ex MethodArgumentNotValidException
   * @return the ResponseEntity<> object
   */
  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    logger.error("MethodArgumentNotValidException: ", ex);
    BindingResult results = ex.getBindingResult();
    List<ObjectError> errors = results.getAllErrors();
    List<String> errorsDetails = new ArrayList<>(10);
    for (ObjectError error : errors) {
      String[] errStr = Objects.requireNonNull(error.getCodes())[0].split("\\.");
      StringBuilder errMess = new StringBuilder();
      errMess.append(errStr[errStr.length - 1]).append(" ").append("incorrect");
      errorsDetails.add(errMess.toString());
    }
    return this.createErrorResponse(
        ResponseStatusCodeEnum.INVALID_PARAMETER, HttpStatus.BAD_REQUEST, errorsDetails);
  }

  /**
   * Handle RuntimeException.
   *
   * @param ex RuntimeException
   * @return the ResponseEntity<> object
   */
  @ExceptionHandler(RuntimeException.class)
  public final ResponseEntity<?> handleAllRunTimeExceptions(RuntimeException ex) {
    logger.error("RuntimeException: ", ex);
    return this.createErrorResponse(
        ResponseStatusCodeEnum.INTERNAL_SERVER_ERROR,
        HttpStatus.INTERNAL_SERVER_ERROR,
        ex.getCause().getMessage());
  }

  /**
   * Handle Exception.
   *
   * @param ex Exception
   * @return the ResponseEntity<> object
   */
  @ExceptionHandler(Exception.class)
  public final ResponseEntity<?> handleAllExceptions(Exception ex) {
    logger.error("Exception: ", ex);
    return this.createErrorResponse(
        ResponseStatusCodeEnum.INTERNAL_SERVER_ERROR,
        HttpStatus.INTERNAL_SERVER_ERROR,
        ex.getMessage());
  }

  private ResponseEntity<Object> createErrorResponse(
      ResponseStatusCodeEnum response, HttpStatus status, List<String> errorsDetails) {
    ResponseStatus responseStatus = new ResponseStatus(response.getCode(), response.getMessage());
    GeneralResponse<Object> responseObject = new GeneralResponse<>();
    responseObject.setData(errorsDetails);
    responseObject.setTimestamp(new Date());
    responseObject.setStatus(responseStatus);
    return new ResponseEntity<>(responseObject, status);
  }

  private ResponseEntity<Object> createErrorResponse(
      ResponseStatusCodeEnum response, HttpStatus status, String message) {
    ResponseStatus responseStatus = new ResponseStatus(response.getCode(), response.getMessage());
    GeneralResponse<Object> responseObject = new GeneralResponse<>();
    responseObject.setData(message);
    responseObject.setStatus(responseStatus);
    responseObject.setTimestamp(new Date());
    return new ResponseEntity<>(responseObject, status);
  }


}
