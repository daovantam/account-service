package com.mor.accountservice.dto;

import com.mor.accountservice.validator.EmailValid;
import com.mor.accountservice.validator.NotNullBlankEmpty;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserRegistrationDto implements Serializable {

  private static final long serialVersionUID = 1L;

  @NotNullBlankEmpty
  private String username;

  @NotNullBlankEmpty
  private String password;

  @NotNullBlankEmpty
  private String identityNo;

  @NotNullBlankEmpty
  private String phone;

  @EmailValid
  @NotNullBlankEmpty
  private String email;
}
