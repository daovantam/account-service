package com.mor.accountservice.dto;

import java.io.File;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MailDto {

  private UUID id;
  private String sendTo;
  private String subject;
  private String content;
  private File attach;
}
