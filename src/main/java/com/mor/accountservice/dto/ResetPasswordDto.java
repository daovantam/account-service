package com.mor.accountservice.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResetPasswordDto {

  private String email;

  private String password;
}
