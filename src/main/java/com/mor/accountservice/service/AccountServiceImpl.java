package com.mor.accountservice.service;

import com.mor.accountservice.client.AuthServiceFeignClient;
import com.mor.accountservice.dto.ChangePasswordDto;
import com.mor.accountservice.dto.ResetPasswordDto;
import com.mor.accountservice.dto.UserDto;
import com.mor.accountservice.dto.UserRegistrationDto;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

  private final String SUBJECT = "Mật khẩu tại house rental đã được khôi phục";
  private final AuthServiceFeignClient authServiceFeignClient;
  private final NotificationService notificationService;

  public AccountServiceImpl(AuthServiceFeignClient authServiceFeignClient,
      NotificationService notificationService) {
    this.authServiceFeignClient = authServiceFeignClient;
    this.notificationService = notificationService;
  }

  @Override
  public UserDto create(UserRegistrationDto user) {
    return Objects.requireNonNull(authServiceFeignClient.createUser(user).getBody()).getData();
  }

  @Override
  public void resetPassword(String username) {
    Map<String, String> params = new HashMap<>();
    ResetPasswordDto result = Objects
        .requireNonNull(authServiceFeignClient.resetPassword(username).getBody()).getData();
    params.put("password", result.getPassword());
    notificationService
        .sendNotification(result.getEmail(), SUBJECT, "reset-password.vm", null, params);
  }

  @Override
  public UserDto changePassword(ChangePasswordDto changePasswordDto) {
    return null;
  }


}
