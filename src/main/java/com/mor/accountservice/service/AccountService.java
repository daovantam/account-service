package com.mor.accountservice.service;

import com.mor.accountservice.dto.ChangePasswordDto;
import com.mor.accountservice.dto.UserDto;
import com.mor.accountservice.dto.UserRegistrationDto;

public interface AccountService {

  /**
   * Invokes Auth Service user creation
   *
   * @param user
   * @return created account
   */
  UserDto create(UserRegistrationDto user);

  void resetPassword(String username);

  UserDto changePassword(ChangePasswordDto changePasswordDto);
}
