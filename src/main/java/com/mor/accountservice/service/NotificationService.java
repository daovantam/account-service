package com.mor.accountservice.service;

import com.mor.accountservice.dto.MailDto;
import java.io.File;
import java.io.StringWriter;
import java.util.Map;
import java.util.UUID;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {

  private final KafkaTemplate<String, MailDto> kafkaTemplate;
  private final VelocityEngine velocityEngine;

  @Autowired
  public NotificationService(
      KafkaTemplate<String, MailDto> kafkaTemplate,
      VelocityEngine velocityEngine) {
    this.kafkaTemplate = kafkaTemplate;
    this.velocityEngine = velocityEngine;
  }

  public void sendNotification(String sendTo, String subject, String template, File attach,
      Map<String, String> params) {
    MailDto mailDto = new MailDto();
    mailDto.setId(UUID.randomUUID());
    mailDto.setSendTo(sendTo);
    mailDto.setSubject(subject);
    mailDto.setContent(getContentFromTemplate(template, params));
    if (attach != null) {
      mailDto.setAttach(attach);
    }
    kafkaTemplate.send("emailNoAttach", mailDto);
  }

  public String getContentFromTemplate(String templateName, Map<String, String> params) {
    Template template = velocityEngine.getTemplate("template/" + templateName);
    VelocityContext context = new VelocityContext();
    for (Map.Entry<String, String> entry : params.entrySet()) {
      context.put(entry.getKey(), entry.getValue());
    }
    StringWriter stringWriter = new StringWriter();
    template.merge(context, stringWriter);
    return stringWriter.toString();
  }
}
