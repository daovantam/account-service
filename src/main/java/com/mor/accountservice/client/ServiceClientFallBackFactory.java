package com.mor.accountservice.client;

import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
final class ServiceClientFallBackFactory implements FallbackFactory<AuthServiceFeignClient> {

  @Override
  public AuthServiceFeignClient create(Throwable cause) {
    throw new RuntimeException(cause);
  }
}
