package com.mor.accountservice.client;

import com.mor.accountservice.dto.ResetPasswordDto;
import com.mor.accountservice.dto.UserDto;
import com.mor.accountservice.dto.UserRegistrationDto;
import com.mor.accountservice.factory.GeneralResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "auth-service", fallbackFactory = ServiceClientFallBackFactory.class)
public interface AuthServiceFeignClient {

  @PostMapping(value = "/uaa/user")
  ResponseEntity<GeneralResponse<UserDto>> createUser(@RequestBody UserRegistrationDto user);

  @GetMapping(value = "/uaa/user/reset-password/{username}")
  ResponseEntity<GeneralResponse<ResetPasswordDto>> resetPassword(
      @PathVariable("username") String username);

}
