package com.mor.accountservice.factory;

import com.mor.accountservice.contant.ResponseStatusCodeEnum;
import java.util.Date;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ResponseFactory {

  public ResponseEntity<?> success(Object data, Class<?> clazz) {
    GeneralResponse<Object> responseObject = new GeneralResponse<>();
    ResponseStatus responseStatus = new ResponseStatus();
    responseStatus.setCode(ResponseStatusCodeEnum.SUCCESS.getCode());
    responseStatus.setMessage(ResponseStatusCodeEnum.SUCCESS.getMessage());
    responseObject.setStatus(responseStatus);
    responseObject.setData(clazz.cast(data));
    responseObject.setTimestamp(new Date());

    return ResponseEntity.ok(responseObject);
  }

  public ResponseEntity<?> success() {
    GeneralResponse<Object> responseObject = new GeneralResponse<>();
    ResponseStatus responseStatus = new ResponseStatus();
    responseStatus.setCode(ResponseStatusCodeEnum.SUCCESS.getCode());
    responseStatus.setMessage(ResponseStatusCodeEnum.SUCCESS.getMessage());
    responseObject.setStatus(responseStatus);
    responseObject.setTimestamp(new Date());
    return ResponseEntity.ok(responseObject);
  }

  public ResponseEntity<?> created(Object data, Class<?> clazz) {
    GeneralResponse<Object> responseObject = new GeneralResponse<>();
    ResponseStatus responseStatus = new ResponseStatus();
    responseStatus.setCode(ResponseStatusCodeEnum.CREATED.getCode());
    responseStatus.setMessage(ResponseStatusCodeEnum.CREATED.getMessage());
    responseObject.setStatus(responseStatus);
    responseObject.setData(clazz.cast(data));
    responseObject.setTimestamp(new Date());

    return new ResponseEntity<Object>(responseObject, HttpStatus.CREATED);
  }
}
